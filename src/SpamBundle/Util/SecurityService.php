<?php

namespace SpamBundle\Util;

use ReCaptcha\ReCaptcha;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service that contains spam prevention methods.
 */
class SecurityService
{
    use ContainerAwareTrait;

    /**
     * Sevice method that checks the reCAPTCHA field.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \ReCaptcha\Response
     */
    public function validateRecaptcha(Request $request)
    {
        $recaptcha = new ReCaptcha($this->container->getParameter('captcha_server_key'));

        return $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
    }
}
