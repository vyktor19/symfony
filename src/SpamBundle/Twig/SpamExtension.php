<?php

namespace SpamBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Twig extension to add ability to get the class name in the twig template.
 */
class SpamExtension extends Twig_Extension
{

    /**
     * The container instance.
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * SpamExtension constructor.
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            'client_key' => new Twig_SimpleFunction('client_key', [$this, 'getClientKey']),
        ];
    }

    /**
     * Get the class name of the object.
     *
     * @return string
     */
    public function getClientKey()
    {
        return $this->container->getParameter('captcha_client_key');
    }
}
