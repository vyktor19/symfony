<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\AuthorTrait;
use VoteBundle\Entity\Traits\VoteTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Thread
 *
 * @ORM\Table(name="thread")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThreadRepository")
 */
class Thread
{
    use VoteTrait;
    use AuthorTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255)
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date")
     */
    private $createdDate;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|\AppBundle\Entity\Post[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="thread", orphanRemoval=true)
     */
    private $posts;

    /**
     * @var int
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views = 0;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|\VoteBundle\Entity\Vote[]
     *
     * @ORM\ManyToMany(targetEntity="VoteBundle\Entity\Vote")
     * @ORM\JoinTable(
     *  name="thread_vote",
     *  joinColumns={
     *      @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="vote_id", referencedColumnName="id")
     *  }
     * )
     */
    private $votes;

    /**
     * Thread constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Thread
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Thread
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Thread
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Get posts
     *
     * @return \AppBundle\Entity\Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Thread
     */
    public function removePost($post)
    {
        $this->posts->remove($post);

        return $this;
    }

    /**
     * Get views.
     *
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set views.
     *
     * @param int $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * Add a view to the count.
     *
     * @return Thread
     */
    public function addView()
    {
        $this->views++;

        return $this;
    }
}
