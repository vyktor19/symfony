<?php

namespace AppBundle\Controller;

use AppBundle\Form\PostType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base controller to display the Categories.
 */
class PostController extends Controller
{

    /**
     * Add Action.
     *
     * Add a new thread.
     *
     * @param int                                       $threadId
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction($threadId, Request $request)
    {
        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $spamService = $this->container->get('security.spam');
            $response = $spamService->validateRecaptcha($request);
            if (!$response->isSuccess()) {
                $captchaError = new FormError('Please validate that you are human.');
                $form->addError($captchaError);
            }

            if ($form->isValid()) {
                /** @var \AppBundle\Entity\Post $post */
                $post = $form->getData();

                $date = DateTime::createFromFormat('U', $request->server->get('REQUEST_TIME'));
                $post->setCreatedDate($date);
                $user = $this->getUser();
                $post->setAuthor($user->getUsername());

                // Set the current thread.
                $em = $this->getDoctrine()->getManager();
                /** @var \AppBundle\Entity\Thread $thread */
                $thread = $em->getRepository('AppBundle:Thread')->find($threadId);
                $post->setThread($thread);

                $em->persist($post);
                $em->flush();

                return $this->redirectToRoute('thread_view', ['id' => $threadId]);
            }
        }

        return $this->render(
            "@theme/post/add.html.twig",
            ['thread_id' => $threadId, 'form' => $form->createView()]
        );
    }
}
