<?php

namespace AppBundle\Controller;

use AppBundle\Form\ThreadType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Base controller to display the Categories.
 */
class ForumController extends Controller
{
    /**
     * List Action
     *
     * Display all the threads.
     * This is the home page.
     *
     * @param string $sortBy
     * @param string $sortOrder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($sortBy = 'id', $sortOrder = 'desc')
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\Thread[] $threads */
        $threads = $em->getRepository('AppBundle:Thread')->getAllSortedBy($sortBy, $sortOrder);

        return $this->render(
            '@theme/thread/list.html.twig',
            ['threads' => $threads]
        );
    }

    /**
     * Add Action.
     *
     * Add a new thread.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(ThreadType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $spamService = $this->container->get('security.spam');
            $response = $spamService->validateRecaptcha($request);
            if (!$response->isSuccess()) {
                $captchaError = new FormError('Please validate that you are human.');
                $form->addError($captchaError);
            }

            if ($form->isValid()) {
                /** @var \AppBundle\Entity\Thread $thread */
                $thread = $form->getData();

                $date = DateTime::createFromFormat('U', $request->server->get('REQUEST_TIME'));
                $thread->setCreatedDate($date);
                $user = $this->getUser();
                $thread->setAuthor($user->getUsername());

                $em = $this->getDoctrine()->getManager();
                $em->persist($thread);
                $em->flush();

                return $this->redirectToRoute('thread_list');
            }
        }

        return $this->render(
            "@theme/thread/add.html.twig",
            ['form' => $form->createView()]
        );
    }

    /**
     * Show Action
     *
     * Display the posts in the selected thread.
     *
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Entity\Thread $thread */
        $thread = $em->getRepository('AppBundle:Thread')->find($id);

        if (is_null($thread)) {
            return $this->redirectToRoute('thread_list');
        }

        $response = null;
        if ($this->memberEligibleForViewIncrease($id, $request)) {
            $thread->addView();
            $em->persist($thread);
            $em->flush();

            $response = $this->setViewsCookie($id, $request);
        }

        return $this->render(
            '@theme/post/list.html.twig',
            ['thread_id' => $id, 'posts' => $thread->getPosts()],
            $response
        );
    }

    /**
     * Helper method to check if the current member is eligible to increase
     * the view count of the thread.
     *
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return bool
     */
    protected function memberEligibleForViewIncrease($id, $request)
    {
        $cookie = $request->cookies->get('views', '');

        $views = explode(',', $cookie);

        foreach ($views as $threadId) {
            if ($threadId === $id) {
                return false;
            }
        }

        return true;
    }

    /**
     * Add the thread id for the thread views count.
     *
     * @param int                                       $id
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function setViewsCookie($id, Request $request)
    {
        $cookie = $request->cookies->get('views', '');

        $cookie .= ','.$id;
        $cookie = trim($cookie, ',');

        $response = new Response();
        $cookie = new Cookie('views', $cookie, strtotime('tomorrow midnight'));

        $response->headers->setCookie($cookie);

        return $response;
    }
}
