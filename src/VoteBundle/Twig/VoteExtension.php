<?php

namespace VoteBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Twig extension to add ability to get the class name in the twig template.
 */
class VoteExtension extends Twig_Extension
{
    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            'class' => new Twig_SimpleFunction('class', [$this, 'getClass']),
        ];
    }

    /**
     * Get the class name of the object.
     *
     * @param object $object
     *
     * @return string
     */
    public function getClass($object)
    {
        return get_class($object);
    }
}
