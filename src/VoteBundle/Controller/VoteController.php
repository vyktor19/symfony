<?php

namespace VoteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VoteBundle\Entity\Vote;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to add and edit vote to entity.
 */
class VoteController extends Controller
{

    /**
     * Action to add vote to entity.
     *
     * @Route("/vote", name="vote")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function voteAction(Request $request)
    {
        $isAjax = $request->isXMLHttpRequest();
        if ($isAjax) {
            $entityType = $request->get('entityType');
            $entityId = $request->get('entityId');
            $vote = $request->get('vote');
            $em = $this->getDoctrine()->getManager();

            /** @var \VoteBundle\Entity\Traits\VoteTrait $entity */
            $entity = $em->find($entityType, $entityId);
            if (method_exists($entity, 'getVotes')) {
                $user = $this->getUser();
                $voteEntity = $entity->getVoteForUser($user->getUsername());

                if (is_null($voteEntity)) {
                    // Create a new vote.
                    $voteEntity = new Vote();
                    $voteEntity->setUser($user->getUsername())
                        ->setVote($vote);
                } elseif ($voteEntity->getVote() === $vote) {
                    // Do not update if the same vote is cast.
                    return new Response($entity->getEntityVotes());
                } else {
                    // Remove the vote entity so the new vote is added.
                    $entity->removeVote($voteEntity);
                    $voteEntity->setVote($vote);
                }

                $entity->addVote($voteEntity);

                $em->persist($voteEntity);
                $em->persist($entity);
                $em->flush();

                return new Response($entity->getEntityVotes());
            }
        }

        return new Response('Error', 400);
    }
}
