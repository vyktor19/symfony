function sendVote(entityType, entityId, vote){
  $.ajax({
    url: '/vote',
    type: "POST",
    dataType: "json",
    data: {entityType: entityType, entityId:entityId, vote: vote},
    async: true,
    success: function(response){
      // Change the total number of votes for the entity.
      $('#vote-'+entityId).text(response);
    }
  })
}