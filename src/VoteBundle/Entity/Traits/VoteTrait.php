<?php

namespace VoteBundle\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use VoteBundle\Entity\Vote;

/**
 * Traits to use in entities to allow voting system to them.
 */
trait VoteTrait
{
    /**
     * Override this to create the link between your entity and the vote entity.
     *
     * @var \Doctrine\Common\Collections\ArrayCollection|\VoteBundle\Entity\Vote[]
     *
     * @ORM\ManyToMany(targetEntity="VoteBundle\Entity\Vote")
     * @ORM\JoinTable(
     *  name="entity_vote",
     *  joinColumns={
     *      @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="vote_id", referencedColumnName="id")
     *  }
     * )
     */
    private $votes;

    /**
     * VoteTrait constructor.
     */
    public function __construct()
    {
        $this->votes = new ArrayCollection();
    }

    /**
     * Get votes for the current Thread.
     *
     * @return \VoteBundle\Entity\Vote[]|\Doctrine\Common\Collections\ArrayCollection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Get up votes for the current entity.
     *
     * @return int
     */
    public function upVotes()
    {
        $votes = $this->getVotes();
        $upVotes = 0;
        foreach ($votes as $vote) {
            if ($vote->getVote() > 0) {
                $upVotes ++;
            }
        }

        return $upVotes;
    }

    /**
     * Get down votes for the current entity.
     *
     * @return int
     */
    public function downVotes()
    {
        $votes = $this->getVotes();
        $downVotes = 0;
        foreach ($votes as $vote) {
            if ($vote->getVote() < 0) {
                $downVotes ++;
            }
        }

        return $downVotes;
    }

    /**
     * Add a vote to the current Thread.
     *
     * @param \VoteBundle\Entity\Vote[]|\Doctrine\Common\Collections\ArrayCollection $votes
     *
     * @return $this
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;

        return $this;
    }

    /**
     * Remove a vote to the entity.
     *
     * @param \VoteBundle\Entity\Vote $vote
     *
     * @return $this
     */
    public function removeVote(Vote $vote)
    {
        $this->votes->removeElement($vote);

        return $this;
    }

    /**
     * Add a vote to the entity.
     *
     * @param \VoteBundle\Entity\Vote $vote
     *
     * @return $this
     */
    public function addVote(Vote $vote)
    {
        $this->votes->add($vote);

        return $this;
    }

    /**
     * Get the vote of the user.
     *
     * @param string $username
     *
     * @return null|\VoteBundle\Entity\Vote
     */
    public function getVoteForUser($username)
    {
        $votes = $this->getVotes();
        foreach ($votes as $vote) {
            if ($vote->getUser() === $username) {
                return $vote;
            }
        }

        return null;
    }

    /**
     * Get the vote count for the entity.
     *
     * @return int
     */
    public function getEntityVotes()
    {
        return $this->upVotes() - $this->downVotes();
    }
}
