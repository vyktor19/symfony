<?php

namespace UserBundle\Controller;

use Symfony\Component\Form\FormError;
use UserBundle\Form\LoginType;
use UserBundle\Form\UserType;
use UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that handles the user security.
 */
class SecurityController extends Controller
{
    /**
     * Register user Action.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $spamService = $this->container->get('security.spam');
            $response = $spamService->validateRecaptcha($request);
            if (!$response->isSuccess()) {
                $captchaError = new FormError('Please validate that you are human.');
                $form->addError($captchaError);
            }

            if ($form->isValid()) {
                $passwordEncoder = $this->container->get('security.password_encoder');
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirect('/');
            }
        }

        return $this->render(
            '@theme/security/register.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Login user action.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $authUtils = $this->container->get('security.authentication_utils');
        // Get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // Last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        $form = $this->createForm(LoginType::class);

        return $this->render('@theme/security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'form' => $form->createView(),
        ]);
    }
}
