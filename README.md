A simple blog created in Symfony.

## General

This is a simple blog created in Symfony 3.3.

The core functionality is used plus the reCAPTCHA PHP client library.

## Functionality provided
* Threads, each containing Posts.
* User registration & login
* Logged in users can add threads and posts
* Logged in users can vote on threads and posts
* Spam prevention methods are used for the user, thread and post create forms: reCAPTCHA and the users need to be registered first.
* The vote functionality is decoupled in a separate bundle, to use just add the `VoteTrait` in the required entity,
  override the `votes` attribute to create the intermediate table (use the attribute in the trait as example)
  and include the `votes.html.twig` template in your entity template.


## Installation
* Run `composer install`
* Update `parameters.yaml` database parameters with your credentials
  * Run `bin/updb.sh` to update your database schema, no data is added by default.
  * OR use the `symfony_block` docker component from the [repository](https://bitbucket.org/vyktor19/docker)
 (change the code mount path: `code_repo_path` variable), and use the database that has some dummy content in it already.
   User/pass combination is admin/admin.
   Host name: symfony.blog.com
   
The parameters default is using the docker mysql container.